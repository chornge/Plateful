package com.chornge.plateful.model;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

import java.io.Serializable;

public class QuakeEvent implements Serializable {

    private Feature quake;

    public QuakeEvent(Feature quake) {
        this.quake = quake;
    }

    public Feature getQuake() {
        return quake;
    }
}
