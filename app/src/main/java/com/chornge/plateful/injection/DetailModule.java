package com.chornge.plateful.injection;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

import com.chornge.plateful.activity.detail.DetailPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailModule {

    @Provides
    DetailPresenter providesDetailPresenter() {
        return new DetailPresenter();
    }
}
