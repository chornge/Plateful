package com.chornge.plateful.injection;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

import com.chornge.plateful.activity.main.MainActivity;

import dagger.Component;

@Component(modules = MainModule.class)
public interface MainComponent {
    void inject(MainActivity activity);
}
