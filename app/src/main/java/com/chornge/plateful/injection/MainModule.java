package com.chornge.plateful.injection;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

import com.chornge.plateful.activity.main.MainPresenter;

import dagger.Module;
import dagger.Provides;

@Module
class MainModule {

    @Provides
    MainPresenter providesMainPresenter() {
        return new MainPresenter();
    }
}
