package com.chornge.plateful.injection;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

import com.chornge.plateful.activity.detail.DetailActivity;

import dagger.Component;

@Component(modules = DetailModule.class)
public interface DetailComponent {
    void inject(DetailActivity activity);
}
