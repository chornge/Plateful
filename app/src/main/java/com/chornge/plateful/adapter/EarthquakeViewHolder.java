package com.chornge.plateful.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chornge.plateful.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

class EarthquakeViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.earthquake_magnitude)
    TextView quake_mag;

    @BindView(R.id.earthquake_date)
    TextView quake_date;

    @BindView(R.id.earthquake_location)
    TextView quake_location;

    EarthquakeViewHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);
    }
}
