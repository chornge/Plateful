package com.chornge.plateful.adapter;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chornge.plateful.R;
import com.chornge.plateful.model.Feature;
import com.chornge.plateful.model.QuakeEvent;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class EarthquakeAdapter extends RecyclerView.Adapter<EarthquakeViewHolder> {

    private List<Feature> quakeList;

    public EarthquakeAdapter(List<Feature> quakeList) {
        this.quakeList = quakeList;
    }

    @Override
    public EarthquakeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_earthquake, parent, false);

        return new EarthquakeViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(EarthquakeViewHolder holder, int position) {
        Feature quake = quakeList.get(position);

        Date date = new Date(quake.getProperties().getTime());

        holder.quake_mag.setText(String.format("%s magnitude", String.valueOf(quake.getProperties().getMag())));
        holder.quake_date.setText(new SimpleDateFormat("EEE, MMM d, ''yy 'at' HH:mm aaa z", Locale.US).format(date));
        holder.quake_location.setText(quake.getProperties().getPlace());

        holder.itemView.setOnClickListener(view -> {
            // communicate from adapter to main activity
            EventBus.getDefault().post(new QuakeEvent(quakeList.get(position)));
        });
    }

    @Override
    public int getItemCount() {
        return this.quakeList.size();
    }
}
