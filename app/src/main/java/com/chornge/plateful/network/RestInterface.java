package com.chornge.plateful.network;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

import com.chornge.plateful.model.EarthQuake;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RestInterface {

    @GET("/earthquakes/feed/v1.0/summary/2.5_day.geojson")
    Call<EarthQuake> getJsonObjects();
}
