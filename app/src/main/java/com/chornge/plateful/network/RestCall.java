package com.chornge.plateful.network;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.util.Log;

import com.chornge.plateful.model.EarthQuake;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Action1;

public class RestCall {

    private static final String TAG = "RestCallTAG_";
    private final RestInterface restInterface;

    public RestCall(RestInterface restInterface) {
        this.restInterface = restInterface;
    }

    @WorkerThread
    public void call(String url, final Action1<EarthQuake> responseAction1) throws Exception {
        try {
            Call<EarthQuake> call = restInterface.getJsonObjects();

            call.enqueue(new Callback<EarthQuake>() {
                @Override
                public void onResponse(@NonNull Call<EarthQuake> call,
                                       @NonNull Response<EarthQuake> responseFromAPI) {
                    Observable.just(responseFromAPI.body()).subscribe(responseAction1);
                }

                @Override
                public void onFailure(@NonNull Call<EarthQuake> call, @NonNull Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "Failed to execute call to fixer api: " + url + ": " + e.toString());
        }
    }
}
