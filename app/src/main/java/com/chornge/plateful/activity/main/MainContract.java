package com.chornge.plateful.activity.main;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

import android.os.Bundle;

import com.chornge.plateful.BasePresenter;
import com.chornge.plateful.BaseView;

public interface MainContract {
    interface View extends BaseView {
        void navigateToDetailActivity(Bundle bundle);
    }

    interface Presenter extends BasePresenter<View> {
        void switchActivities(Bundle bundle);
    }
}
