package com.chornge.plateful.activity.main;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

import android.os.Bundle;

public class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;

    @Override
    public void addView(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void removeView() {
        this.view = null;
    }

    @Override
    public void switchActivities(Bundle bundle) {
        view.navigateToDetailActivity(bundle);
    }
}
