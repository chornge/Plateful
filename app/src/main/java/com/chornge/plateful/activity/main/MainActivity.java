package com.chornge.plateful.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chornge.plateful.R;
import com.chornge.plateful.activity.detail.DetailActivity;
import com.chornge.plateful.adapter.EarthquakeAdapter;
import com.chornge.plateful.injection.DaggerMainComponent;
import com.chornge.plateful.model.EarthQuake;
import com.chornge.plateful.model.Feature;
import com.chornge.plateful.model.QuakeEvent;
import com.chornge.plateful.network.RestCall;
import com.chornge.plateful.network.RestInterface;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.functions.Action1;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    @BindView(R.id.no_earthquakes_text)
    TextView noEarthquakesText;

    @BindView(R.id.earthquake_recycler)
    RecyclerView recyclerView;

    @Inject
    MainPresenter presenter;

    private RestCall restCall;
    private List<Feature> listOfAllEarthquakes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DaggerMainComponent.create().inject(this);
        ButterKnife.bind(this);

        presenter.addView(this);

        String url = "https://earthquake.usgs.gov/";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestInterface restInterface = retrofit.create(RestInterface.class);

        restCall = new RestCall(restInterface);

        getResponseFromAPI(url, earthQuake -> {
            listOfAllEarthquakes.clear();
            listOfAllEarthquakes.addAll(earthQuake.getFeatures());

            if (listOfAllEarthquakes.size() == 0) {
                noEarthquakesText.setVisibility(View.VISIBLE);
            } else {
                noEarthquakesText.setVisibility(View.GONE);

                EarthquakeAdapter adapter = new EarthquakeAdapter(listOfAllEarthquakes);
                recyclerView.setHasFixedSize(true);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(adapter);
            }
        });
    }

    public void getResponseFromAPI(String url, final Action1<EarthQuake> resultAction1) {
        try {
            restCall.call(url, results ->
                    Observable.just(results).subscribe(resultAction1));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveEvent(QuakeEvent event) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("detailKey", event.getQuake());
        presenter.switchActivities(bundle);
    }

    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);
        presenter.addView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);
        presenter.removeView();
    }

    @Override
    public void navigateToDetailActivity(Bundle bundle) {
        Intent navigateIntent = new Intent(getApplicationContext(), DetailActivity.class);
        navigateIntent.putExtras(bundle);
        startActivity(navigateIntent);
    }

    @Override
    public void showError(String str) {
        //
    }
}
