package com.chornge.plateful.activity.detail;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.chornge.plateful.R;
import com.chornge.plateful.injection.DaggerDetailComponent;
import com.chornge.plateful.model.Feature;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity implements DetailContract.View {

    @Inject
    DetailPresenter presenter;

    @BindView(R.id.earthquake_title)
    TextView title;

    @BindView(R.id.earthquake_magnitude)
    TextView magnitude;

    @BindView(R.id.earthquake_date)
    TextView earthquakeDate;

    @BindView(R.id.earthquake_long_lat)
    TextView longLat;

    @BindView(R.id.earthquake_location)
    TextView location;

    @BindView(R.id.earthquake_tsunami_potential)
    TextView tsunamiPotential;

    @BindView(R.id.earthquake_type)
    TextView type;

    @BindView(R.id.earthquake_status)
    TextView status;

    @BindView(R.id.last_update)
    TextView lastUpdate;
    private Feature quake;

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        DaggerDetailComponent.create().inject(this);
        ButterKnife.bind(this);

        presenter.addView(this);

        quake = (Feature) getIntent().getExtras().getSerializable("detailKey");

        presenter.setTexts();
    }

    @Override
    protected void onStop() {
        super.onStop();

        presenter.removeView();
    }

    @Override
    public void showAllTexts() {
        if (quake != null) {
            Date date = new Date(quake.getProperties().getTime());
            Date date2 = new Date(quake.getProperties().getUpdated());

            title.setText(quake.getProperties().getTitle());
            magnitude.setText(String.format("%s magnitude", String.valueOf(quake.getProperties().getMag())));
            earthquakeDate.setText(new SimpleDateFormat("EEE, MMM d, ''yy 'at' HH:mm aaa z", Locale.US).format(date));

            longLat.setText(String.format("%s , %s (long, lat)",
                    String.valueOf(quake.getGeometry().getCoordinates().get(0)),
                    String.valueOf(quake.getGeometry().getCoordinates().get(1))));
            location.setText(String.valueOf(quake.getProperties().getPlace()));

            tsunamiPotential.setText(String.format("%sperc chance of tsunami",
                    String.valueOf(quake.getProperties().getTsunami() * 100)));
            type.setText(quake.getProperties().getType());
            status.setText(String.format("Status: %s", quake.getProperties().getStatus()));

            lastUpdate.setText(String.format("Last Updated %s",
                    new SimpleDateFormat("EEE, MMM d, ''yy 'at' HH:mm aaa z", Locale.US).format(date2)));
        }
    }

    @Override
    public void showError(String str) {
        //
    }
}
