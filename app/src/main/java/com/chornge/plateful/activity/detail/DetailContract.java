package com.chornge.plateful.activity.detail;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

import com.chornge.plateful.BasePresenter;
import com.chornge.plateful.BaseView;

public interface DetailContract {
    interface View extends BaseView {
        void showAllTexts();
    }

    interface Presenter extends BasePresenter<DetailContract.View> {
        void setTexts();
    }
}
