package com.chornge.plateful.activity.detail;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

public class DetailPresenter implements DetailContract.Presenter {

    private DetailContract.View view;

    @Override
    public void addView(DetailContract.View view) {
        this.view = view;
    }

    @Override
    public void removeView() {
        this.view = null;
    }

    @Override
    public void setTexts() {
        view.showAllTexts();
    }
}
