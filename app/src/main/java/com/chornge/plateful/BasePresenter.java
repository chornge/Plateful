package com.chornge.plateful;

/*
 * Created by xtianmbaba on 2/27/2018.
 */

public interface BasePresenter<V extends BaseView> {
    void addView(V v);

    void removeView();
}
